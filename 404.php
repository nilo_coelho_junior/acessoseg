<?php get_header(); ?>
<div class="container wrapper text-center">
	<div class="col-lg-12 text-center">
		<h1>Ops! Essa página não existe</h1>
		<br>
		<br>
		<a href="<?php echo home_url(); ?>" class="btn btn-primary btn-lg">Quero conhecer a Acesso Seguros</a>
	</div>
</div>
<?php get_footer(); ?>