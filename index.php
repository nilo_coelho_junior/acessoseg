<?php get_header(); ?>

<div class="container-fluid blog featured" id="blog">
  <div class="container">
    <div class="col-lg-12">
      <h1 class="text-center title">
        <strong>Blog</strong>
      </h1>
      <div class="row content-box masonry" id="wp-infinity-scroll">
        <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'templates/item-blog', get_post_format() ); ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
      <br>
      <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div id="nav-below" class="navigation col-lg-12 center-block">
          <div class="col-lg-6 text-right"><?php next_posts_link( __( '<button class="btn btn-default btn-lg">&larr; Posts Antigos</button>', 'twentyten' ) ); ?></div>
          <div class="col-lg-6"><?php previous_posts_link( __( '<button class="btn btn-default btn-lg">Posts Recentes &rarr;</button>', 'twentyten' ) ); ?></div>
        </div><!-- #nav-below -->
      <?php endif; ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>