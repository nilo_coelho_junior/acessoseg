<?php

include_once 'includes/ajax-response.php';

/*REMOVE ADMIN BAR*/
add_filter('show_admin_bar', '__return_false');

/*INSERT STYLE, SCRIPTS*/

function site_style() {
	wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/dist/css/style.css');
}

add_action( 'wp_enqueue_styles', 'site_style' );

function site_scripts() {
	wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/dist/css/style.css');
	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/dist/js/main.js');
}

add_action( 'wp_enqueue_scripts', 'site_scripts' );

/* FIPE */
add_action( 'wp_ajax_get_marcas', 'get_marcas' );
add_action( 'wp_ajax_nopriv_get_marcas', 'get_marcas' );

function get_marcas() {
	$parallelum = file_get_contents("https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas");
	echo $parallelum;
	die();
}

add_action( 'wp_ajax_get_modelos', 'get_modelos' );
add_action( 'wp_ajax_nopriv_get_modelos', 'get_modelos' );

function get_modelos() {
	$parallelum = file_get_contents("https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/".$_POST["marca"]."/modelos");
	echo $parallelum;
	die();
}

add_action( 'wp_ajax_get_anos', 'get_anos' );
add_action( 'wp_ajax_nopriv_get_anos', 'get_anos' );

function get_anos() {
	$parallelum = file_get_contents("https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/".$_POST[marca]."/modelos/".$_POST[modelo]."/anos");
	echo $parallelum;
	die();
}

add_action( 'wp_ajax_get_valor', 'get_valor' );
add_action( 'wp_ajax_nopriv_get_valor', 'get_valor' );

function get_valor() {
	$parallelum = file_get_contents("https://fipe-parallelum.rhcloud.com/api/v1/carros/marcas/".$_POST[marca]."/modelos/".$_POST[modelo]."/anos/".$_POST[ano]);
	echo $parallelum;
	die();
}

add_action( 'wp_ajax_get_cep', 'get_cep' );
add_action( 'wp_ajax_nopriv_get_cep', 'get_cep' );

function get_cep() {
	echo file_get_contents("https://viacep.com.br/ws/".$_POST[cep]."/json/");
	die();
}

add_action( 'wp_ajax_send_cotation', 'send_cotation' );
add_action( 'wp_ajax_nopriv_send_cotation', 'send_cotation' );

function send_cotation() {
	$form = $_POST['data'];
	

	if ( empty($form) || !wp_verify_nonce($form[0]["value"],'send_cotation') ) {
			ajaxResponse(400, "Requisição inválida");
	    die();
	} else {
		require_once 'includes/wpbp-send-cotation-by-email.php';

		$send = new WP_Example_Process();

		$body = cotation_to_email($form);
		$admin_email = get_option( 'admin_email' );
		$headers = array('Content-Type: text/html; charset=UTF-8');
		array_push($headers, 'From: '.get_option('admin_email'));
		
		$send->push_to_queue(array('body' => $body, 'headers' => $headers, 'to' => $admin_email));
		$send->save()->dispatch();

		ajaxResponse(302, get_permalink(get_page_by_path('recebemos-sua-cotacao')));
		die();
	}
}

function cotation_to_email($cotation){
	$content = "<h2>Cotação Seguro ".ucfirst("auto")."</h2><hr>";
	$principalCondutor = "Sim";
	foreach ($cotation as $key => $value) {
		if ($value["name"] != "token" && $value["name"] != "_wp_http_referer" && $value["name"] != "page"){
			if ($value["name"] == "ehPrincipalCondutor" && $value["value"] == "Não"){
				$principalCondutor = "Não";	
			}

			if ($principalCondutor == "Sim" && in_array($value["name"], array("Quem é o principal condutor?", "nomeCondutor", "cpfCondutor", "sexoCondutor", "dataNascimentoCondutor", "profissaoCondutor", "anoPrimeiraHabilitacao")) ){

			}else{
				if ($value["name"] == "section"){
					$content.= "<h3>".$value["value"]."</h3>";
				}else{
					$content.="<p>";
					$string = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $value["name"]); 
					$string = trim($string);
					$content.="<strong>".$string.": </strong>";
					$content.=$value["value"]."</p>"; 
				}
			}
		}
	}
	return $content;
}


add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');

function my_wpcf7_ajax_loader () {
    return  get_template_directory_uri().'/assets/dist/img/ellipsis.gif';
}

add_filter( 'the_content', 'remove_autop_for_seguro', 0 );

function remove_autop_for_seguro( $content )
{
     global $post;

     // Check for single page and image post type and remove
     if ( $post->post_type == 'seguro' )
          remove_filter('the_content', 'wpautop');

     return $content;
}

remove_filter ('the_exceprt', 'wpautop');

function custom_excerpt_length( $length ) {
    return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length');

function excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '[...]';
	} else {
		echo $excerpt;
	}
}

function classInsurances($count){
	if ($count == 1) {
		return "col-lg-12";	
	}else if ($count == 2) {
		return "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	}else if ($count == 3 or $count == 5 or $count == 6 or $count%2 > 0) {
		return "col-lg-4 col-md-4 col-sm-4 col-xs-12";
	}else if ($count == 4 or $count%2 == 0) {
		return "col-lg-3 col-md-3 col-sm-6 col-xs-12";
	}
}

function filter_types_child_posts($posts){
	return $posts->fields['visivel-na-home'] == 1;
}
?>