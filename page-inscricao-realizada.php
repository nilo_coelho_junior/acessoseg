<?php 
get_header(); 
?>

<div class="container-fluid wrapper primary-2 text-center">
	<img class="flat" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/newsletter.png" alt="">
	<h1><strong>INSCRIÇÃO REALIZADA COM SUCESSO!</strong></h1>
	<div class="container">
		<br>
		<p class="text-left">Agora você tem acesso ao nosso <strong>conteúdo exclusivo!</strong> Enviaremos para você, via e-mail, nossas promoções, novidades, dicas sobre como utilizar seus seguros e outras informações exclusivas para facilitar sua vida de segurado.</p>

		<p class="text-left">O primeiro e-mail deve chegar a qualquer momento! Caso não encontre, pode ser que esteja na pasta Lixeira ou Spam. Lembre-se de nos marcar como <strong>remetente confiável</strong> para garantir que você não perderá alguma dica valiosa sobre importação!</p>
	</div>
</div>

<?php include_once "templates/faq.php"; ?>

<?php get_footer(); ?>