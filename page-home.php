<?php get_header(); ?>

<div class="container">
  <div class="col-lg-12">
    <?php putRevSlider("home") ?>
  </div>
</div>

<div class="container-fluid wrapper carousel">
  <div class="carousel-insurers">
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/allianz.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/azul.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/bradesco.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/hdi.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/itau-logo.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/liberty.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/mapfre.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/nobre.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/porto-seguro.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/sulamerica.jpg">
    </div>
    <div>
      <img alt="" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/TOKIO_MARINE_LOGO.jpg">
    </div>
  </div>
</div>

<?php 
  $query = new WP_Query(array('post_type' => 'seguro', 'name' => 'auto' ));

  if ($query->have_posts()): 
    while($query->have_posts()) : $query->the_post();  
?>

<div class="container-fluid wrapper success insure-auto" id="seguro-auto">
  <div class="container">
    <h1 class="text-center title col-lg-12">Por quê ter um Seguro Auto?</h1>
    <p class="text-center col-lg-10 col-lg-offset-1 text"><?php echo the_content(); ?></p>
    
    <?php 
      $coberturas = types_child_posts('cobertura');
      $filtered = array_filter($coberturas, "filter_types_child_posts");
      $count = count($filtered);
      $index = 0;
    ?>
    <h2 class="text-center col-lg-12 subtitle">Coberturas e Assistências</h2>
    <div class="row content-box">
      <?php foreach ($filtered as $cobertura) {  ?>
      <div class="<?php echo classInsurances($count); ?> item text-center">
        <?php echo get_the_post_thumbnail( $cobertura->ID, 'small', Array('class' => 'flat', 'height' => '100')); ?>
        <h3><?php echo $cobertura->post_title; ?></h3>
      </div>
      <?php } ?>
    </div>
    <?php 
      $child = get_children(array('post_parent' => get_the_id(), 'post_type' => 'seguro'));
      $child = array_shift($child);
     ?>
    <div class="col-lg-4 col-lg-offset-4 text-center">
      <a class="btn btn-lg btn-warning" href="<?php echo the_permalink(); ?>">Quero conhecer mais coberturas</a>
    </div>
  </div>
</div>
<?php 
    endwhile;
    wp_reset_postdata(); 
  endif;
?>

<div class="container-fluid wrapper why" id="porque-acesso">
  <div class="container">
    <h1 class="text-center title">Porque a Acesso Seguros?</h1>
    <div class="carousel-why hidden-lg hidden-md">
      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/certificado.png" alt="">
        <br><br>
        <h3>Experiência</h3>
        <br>
        <p class="text-justify">Aqui, você conta com um time de diretores com mais de 27 anos de experiência atuando como executivos no mercado segurador e como corretores de seguro. E o que isso significa? Que estamos prontos para te ajudar quando surgir uma dúvida e facilitar a sua experiência na hora de contratar o seu seguro.</p>
      </div>
      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/price.png" alt="">
        <br><br>
        <h3>Preço</h3>
        <br>
        <p class="text-justify">Na hora de contratar o seu seguro, você escolhe as coberturas que realmente quer e nós buscamos dentre as maiores Cias Seguradoras, o melhor preço. Tudo isso, para que você tenha um seguro do seu jeito sem pagar a mais. Aqui na Acesso, somos sempre transparentes, você merece isso! </p>
      </div>
      <div>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/attendance.png" alt="">
        <br><br>
        <h3>Atendimento</h3>
        <br>
        <p class="text-justify">A sua satisfação é um compromisso de máxima importância para nós. Aqui, você será sempre lembrado e não só na hora da sua renovação. Temos uma relação de parceria com nossos clientes, nós te avisamos com antecedência do vencimento do seu seguro. A nossa equipe está pronta para te dar todo suporte necessário, além de ser fácil nos encontrar pelo celular a qualquer hora. E se quiser nos visitar, estamos a sua disposição de segunda a sexta durante o horário de 9hs as 18hs.</p>
      </div>
    </div>
    
    <div class="row content-box visible-lg visible-md">
      <div class="col-lg-2 col-md-2">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/certificado.png">
      </div>
      <div class="col-lg-10 col-md-10">
        <h3>Experiência</h3>
        <p>Aqui, você conta com um time de diretores com mais de 27 anos de experiência atuando como executivos no mercado segurador e como corretores de seguro. E o que isso significa? Que estamos prontos para te ajudar quando surgir uma dúvida e facilitar a sua experiência na hora de contratar o seu seguro.</p>
      </div>
    </div>
    <div class="row content-box visible-lg visible-md">
      <div class="col-lg-2 col-md-2">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/price.png">
      </div>
      <div class="col-lg-10 col-md-10">
        <h3>Preço</h3>
        <p>Na hora de contratar o seu seguro, você escolhe as coberturas que realmente quer e nós buscamos dentre as maiores Cias Seguradoras, o melhor preço. Tudo isso, para que você tenha um seguro do seu jeito sem pagar a mais. Aqui na Acesso, somos sempre transparentes, você merece isso! </p>
      </div>
    </div>
    <div class="row content-box visible-lg visible-md">
      <div class="col-lg-2 col-md-2">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/attendance.png">
      </div>
      <div class="col-lg-10 col-md-10">
        <h3>Atendimento</h3>
        <p>A sua satisfação é um compromisso de máxima importância para nós. Aqui, você será sempre lembrado e não só na hora da sua renovação. Temos uma relação de parceria com nossos clientes, nós te avisamos com antecedência do vencimento do seu seguro. A nossa equipe está pronta para te dar todo suporte necessário, além de ser fácil nos encontrar pelo celular a qualquer hora. E se quiser nos visitar, estamos a sua disposição de segunda a sexta durante o horário de 9hs as 18hs.</p>
      </div>
    </div>
  </div>
</div>

<?php 
  $query = new WP_Query(array('post_type' => 'seguro', 'post_parent' => 0, 'order' => 'ASC'));
  $count = $query->post_count;
  if ($query->have_posts()):
?>
<div class="container-fluid wrapper default insurances" id="seguros">
  <div class="container">
    <h1 class="text-center title col-lg-12">Seguros</h1>
    
    <div class="row-fluid content-box hidden-xs">
      <?php while($query->have_posts()) : $query->the_post(); ?>
      <div class="<?php echo classInsurances($count); ?> item text-center">
        <a href="<?php echo get_permalink(); ?>">
          <h4 class="title"><?php echo the_title(); ?></h4>
          <?php echo do_shortcode('[types field="icone" id="{the_id()}" class="flat"]') ?>
        </a>
      </div>
    <?php endwhile; ?>
    </div>

    <div class="carousel-insurances visible-xs">
      <?php while($query->have_posts()) : $query->the_post(); ?>
        <div>
          <a href="<?php echo get_permalink(); ?>">
            <h4 class="title"><?php echo the_title(); ?></h4>
            <?php echo do_shortcode('[types field="icone" id="{the_id()}" class="flat"]') ?>
          </a>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</div>
<?php endif; ?>

<?php 
   $query = new WP_Query(array('post_type' => 'depoimento', 'orderby' => 'date'));

    if ($query->have_posts()):
?>

<div class="container-fluid wrapper testimonial" id="depoimentos">
  <div class="container">
    <h1 class="text-center title">Depoimentos</h1>
    <div class="carousel-testimonial">
      <?php while($query->have_posts()) : $query->the_post(); ?>
      <div class="item text-center">
        <p><?php the_content(); ?></p>
         <?php the_post_thumbnail('full', array('class' => 'flat')); ?>
        <h4 class="title"><?php the_title(); ?></h4>
        <p class="subtitle"><?php echo do_shortcode('[types field="cargo"]') ?></p>
      </div>
    <?php 
      endwhile; 
      wp_reset_query();
    ?>
    </div>
  </div>
</div>
<?php endif; ?>


<div class="container-fluid wrapper blog" id="blog">
  <div class="container">
    <div class="col-lg-6">
      <h1 class="text-center title col-lg-12">As mais lidas do Blog</h1>
      <div class="row content-box">
        <?php 
          $query = new WP_Query(array('post_type' => 'post', 'orderby' => 'date', 'posts_per_page' => 3));
          
          if ($query->have_posts()):

            while($query->have_posts()) : $query->the_post();
        ?>

        <div href="" class="col-lg-12 item">
          <a href="<?php the_permalink(); ?>">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs image-feature">
                <?php the_post_thumbnail('thumbnail'); ?>
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-feature">
                <h4 class="title"><?php the_title(); ?></h4>
                <p class="excerpt"><?php echo excerpt_max_charlength(125); ?></p>
              </div>
            </div>
          </a>
        </div>
        <?php 
            endwhile;
            wp_reset_query();
          endif;
        ?>

        <div class="col-lg-12 text-center">
          <a class="btn btn-primary btn-lg" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">Mais Notícias</a>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <hr class="hidden-lg">
      <h1 class="text-center title col-lg-12">Vídeos</h1>      
      <?php echo do_shortcode('[embedyt] http://www.youtube.com/embed?layout=gallery&amp;listType=playlist&amp;list=UUmjPXvKtutlY8XejEY9RoSA[/embedyt]');
       ?>
      <div class="col-lg-12 text-center">
        <a class="btn btn-danger btn-lg" href="https://www.youtube.com/channel/UCmjPXvKtutlY8XejEY9RoSA">Mais Vídeos</a>
      </div>
    </div>
  </div>
</div>

<?php include_once "templates/faq.php"; ?>

<div class="container-fluid wrapper contact" id="contato">
  <div class="container">
    <h1 class="text-center title">Fale Conosco</h1>
    <div class="col-lg-6">

      <p><span class="icon-envelope"></span>atendimento@acessoseg.com</p>
      <p><span class="icon-phone"></span>0800 086 6464</p>
      <p><span class="icon-whatsapp-logo"></span>(86) 99800 7000</p>

      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7948.218331002753!2d-42.76507319673193!3d-5.086043606444062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x78e3a0591894c57%3A0xa821824e24069433!2sAcesso+Seguros!5e0!3m2!1spt-BR!2sbr!4v1473139745242" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-lg-6">
      <h3>Nos envie um e-mail</h3>
      <?php echo do_shortcode('[contact-form-7 id="31" title="Contato"]'); ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>