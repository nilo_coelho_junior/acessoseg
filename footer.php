    <div id="footer" class="container-fluid wrapper footer">
      <div class="container">

        <div class="col-lg-4">

          <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo-bg-white.png" class="logo">
          
          <div class="row">
            <div class="col-lg-1">
              <span class="icon-envelope"></span>
            </div>
            <div class="col-lg-11">
              <p>atendimento@acessoseg.com</p>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-1">
              <span class="icon-phone"></span>
            </div>
            <div class="col-lg-11">
              <p>0800 086 6464</p>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-1">
              <span class="icon-whatsapp-logo"></span>
            </div>
            <div class="col-lg-11">
              <p>(86) 99800 7000</p>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-1">
              <span class="icon-mapmarker"></span>
            </div>
            <div class="col-lg-11">
              <p>
                Rua Governador Gayoso de Almendra, 548 <br> 
                São João, Teresina - PI <br> 
                64046-455
              </p>
            </div>
          </div>
        </div>

        <div class="col-lg-2">
          <h3><strong>Seguros</strong></h3>
          <?php
            $query = new WP_Query(array('post_type' => 'seguro', 'post_parent' => 0, 'order' => 'ASC'));
            $count = $query->post_count;
            if ($query->have_posts()):
              while($query->have_posts()) : $query->the_post();
          ?>
                <p>
                  <a href="<?php echo get_permalink(); ?>">
                    <?php echo the_title(); ?>
                  </a>
                </p>
          <?php 
              endwhile; 
            endif;
          ?>
        </div>
        <div class="col-lg-3 newsletter">
          <h3 class="title"><strong>Cadastre-se na nossa lista VIP</strong></h3>
          <?php echo do_shortcode('[mc4wp_form id="2508"]' ); ?>      
        </div>
        <div class="col-lg-3">
          <div class="row">
            <div class="col-lg-12 facebook-plugin"></div>
          </div>
        </div>
      </div>
    </div>

    <?php wp_footer(); ?>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
  </div>
</body>