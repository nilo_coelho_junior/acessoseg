function wizardButtonsNameEmpresarial(index){
	switch (index) {
		case 0:
			$('.cotacao .previous').addClass('disabled')
			$('.cotacao .next').removeClass('disabled')
			break;
		case 1:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').removeClass('disabled')
			$('.cotacao .next').text("Solicitar Cotação")
			break;
		default:
			break;
	}
}

function voltaMaxDate(){
	var ida = $("input[name='dataIda']").val();
	var i_day = ida.split("/")[0];
	var i_moth = ida.split("/")[1];
	var i_year = ida.split("/")[2];
	var date = new Date(i_year, i_moth, i_day);
	console.log(date);
	return date;
}

$(function() {

	$(".cotacao.empresarial").formValidation({
		framework: "bootstrap",
		locale: "pt_BR",
	});
});