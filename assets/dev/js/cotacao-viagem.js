function wizardButtonsNameViagem(index){
	console.log(index);
	switch (index) {
		case 0:
			$('.cotacao .previous').addClass('disabled')
			break;
		case 1:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').removeClass('disabled').text("Solicitar Cotação")
			break;
		default:
			break;
	}
}

function voltaMaxDate(){
	var ida = $("input[name='dataIda']").val();
	var i_day = ida.split("/")[0];
	var i_moth = ida.split("/")[1];
	var i_year = ida.split("/")[2];
	var date = new Date(i_year, i_moth, i_day);
	return date;
}

$(function() {

	$(".cotacao.viagem").formValidation({
		framework: "bootstrap",
		locale: "pt_BR",
		fields: {
			dataIda: {
				validators:{
					date:{
						message: "A data de ida não pode ser anterior a hoje",
						format: "DD/MM/YYYY",
						min: new Date()
					}
				}
			},
			dataVolta: {
				validators:{
					date:{
						message: "A data de volta não pode ser anterior a hoje",
						format: "DD/MM/YYYY",
						min: new Date()
					},
					callback: {
						message: "A data de volta não pode ultrapassar 119 dias apartir da data de ida",
						callback: function (value, validator, $field) {
							var dataIda = $('input[name="dataIda"]').val();
							var ida = moment(dataIda, "DD/MM/YYYY");
							var volta = moment(value, "DD/MM/YYYY");
							if (ida.add(119, 'days').isSameOrAfter(volta)){
								return true;
							}else {
								return false;
							}
						}
					}
				}
			},
		}
	}).on('change', '[name="atividadeComercialNoImovel"]', function (e) {
		var fv = $(".cotacao.condominio").data("formValidation");
		var taxi = $("input[name='atividadeComercialNoImovel']:checked").val();

		if (taxi == "Sim") {
			fv.enableFieldValidators('desejaSeguroParaEquipamentos', true).revalidateField('desejaSeguroParaEquipamentos');
			$(".desejaSeguroParaEquipamentos").removeClass('hide')
		} else {
			fv.enableFieldValidators('desejaSeguroParaEquipamentos', false).revalidateField('desejaSeguroParaEquipamentos');
			$(".desejaSeguroParaEquipamentos").addClass('hide')
		}
	})
});