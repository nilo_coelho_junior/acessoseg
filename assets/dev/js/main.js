function resizeSteps() {
	var size = 100/$(".steps li:visible").length;

	$(".steps li").each(function(index, el) {
		$(el).css('width', size+"%");	
	});
}

function getAge(dateString) {
  return moment().diff(moment(dateString, "D/M/YYYY"), 'years');
}

function maxAge () {
	var date = new Date()
	var year = date.getFullYear()-18
	var month = date.getMonth()+1
	var day = ("0" + date.getDate()).slice(-2)
	return day+"/"+month+"/"+year
}

function minAge () {
	var date = new Date()
	var year = date.getFullYear()-101
	var month = date.getMonth()+1
	var day = ("0" + date.getDate()).slice(-2)
	return day+"/"+month+"/"+year
}

function validarCPF(inputCPF){
    var soma = 0;
    var resto;
    
    if(inputCPF == '00000000000') return false;
    for(i=1; i<=9; i++) soma = soma + parseInt(inputCPF.substring(i-1, i)) * (11 - i);
    resto = (soma * 10) % 11;

    if((resto == 10) || (resto == 11)) resto = 0;
    if(resto != parseInt(inputCPF.substring(9, 10))) return false;

    soma = 0;
    for(i = 1; i <= 10; i++) soma = soma + parseInt(inputCPF.substring(i-1, i))*(12-i);
    resto = (soma * 10) % 11;

    if((resto == 10) || (resto == 11)) resto = 0;
    if(resto != parseInt(inputCPF.substring(10, 11))) return false;
    return true;
}

function baseUrl(){
  if (location.host == "localhost" || location.host == "127.0.0.1"){
    return "/acessoseg/"
  }else{
    return "/"
  }
}

function insertFacebookPlugin () {
  $(".facebook-plugin").html('<div class="fb-page" data-href="https://www.facebook.com/acessoseguros/" data-tabs="timeline, messages" data-width="'+$(".facebook-plugin").outerWidth()+'" data-height="'+$("#footer .container").height()+'" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/acessoseguros/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/acessoseguros/">Acesso Seguros</a></blockquote></div>')
}

$(function() {
  $(".load-page").removeClass('fadeIn').addClass('fadeOut');
  $(".main-page").addClass('fadeIn');

  $('.carousel-insurers').slick({
    slidesToShow: 10,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: false,
    responsive: [
	    {
	      breakpoint: 1100,
	      settings: {
	        slidesToShow: 8,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 868,
	      settings: {
	        slidesToShow: 6,
	        slidesToScroll: 6
	      }
	    },
	    {
	      breakpoint: 700,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4
	      }
	    },
	    {
	      breakpoint: 500,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3
	      }
	    },
	     {
	      breakpoint: 320,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	  ]
  });
  
  $('.carousel-testimonial').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: true,
    dots: true,
    responsive: [
	    {
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	      	arrows: false,
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
  });
  
  $('.carousel-why').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,
    arrows: false,
    dots: true
  });

  $('.carousel-insurances').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: false,
    dots: true
  });

  $('.carousel-page-insurances').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: false,
    dots: true
  });
 
  $('body').scrollspy({ target: '#navbar', offset: 50});

  $("#navbar a").on('click', function(event) {
	  // Make sure this.hash has a value before overriding default behavior
	  if (this.hash !== "") { 	    
	    // Store hash
	    var hash = this.hash;

	    // Using jQuery's animate() method to add smooth page scroll
	    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
	    $('html, body').animate({
	      scrollTop: $(hash).offset().top-50
	    }, 800, function(){

	    // Add hash (#) to URL when done scrolling (default click behavior)
	      window.location.hash = hash;
	    });

	  } // End if
	});

  $('[data-mask="cep"]').keyup(function(event) {
    var fieldName = event.currentTarget.name;
  	var cep = event.target.value.replace("-", "");
    var fv = $(".cotacao").data("formValidation");
  	var re = /(\d{8})/g
  	if (re.test(cep)){
  		$.post(baseUrl()+"wp-admin/admin-ajax.php", {action : 'get_cep', cep: cep}, function(data){
  			data = JSON.parse(data);
        if(data.logradouro === undefined){
          fv.updateMessage(fieldName, "zipCode", "CEP inválido")
          fv.updateStatus(fieldName, "INVALID", "zipCode")
          $(event.target).parent().parent().find(".cep-result").html("");
        }else{
          var address = data.logradouro+" "+data.complemento+", "+data.bairro+". "+data.localidade+"-"+data.uf
          fv.updateStatus(fieldName, "VALID")
          $(event.target).parent().parent().find(".cep-result").html(address.toUpperCase())
        }
			});
  	}
  });

  $('[data-mask="cep-autocomplete"]').keyup(function(event) {
    var cep = event.target.value.replace("-", "");
    var re = /(\d{8})/g
    if (re.test(cep)){
      $.post(baseUrl()+"wp-admin/admin-ajax.php", {action : 'get_cep', cep: cep}, function(data){
        data = JSON.parse(data);
        $('input[name="bairro"]').val(data.bairro);
        $('input[name="endereco"]').val(data.logradouro);        
        $('input[name="cidade"]').val(data.localidade);
        $('select[name="estado"]').val(data.uf);
      });
    }
  });

  $('.masonry').masonry({
  	itemSelector: '.item',
  	columnWidth: '.item',
  	percentPosition: true
  });

  $('[data-toggle="tooltip"]').tooltip();

  resizeSteps();

  insertFacebookPlugin();
});