$(function() {
	$("[data-mask='currency']").inputmask({
    alias: "currency",
    groupSeparator: ".",
    radixPoint: ",",
    digits: 2,
    autoGroup: true,
    prefix: "R$ "
  });

  $("[data-mask='date']").inputmask({
    alias: "dd/mm/yyyy",
    placeholder: "dd/mm/aaaa"    
  });

  $("[data-mask='cep']").inputmask({
  	alias: "number",
  	mask: "99999-999"
  });

  $("[data-mask='cpf']").inputmask({
    alias: "number",
    mask: "999.999.999-99"
  });

  $("[data-mask='phone']").inputmask({
    mask: "(99) 9999[9].9999"
  });

  $("[data-mask='board'").inputmask({
    mask: "aaa-9999"
  })

  $("[data-mask='year']").inputmask({
    mask: "9999"
  });
})