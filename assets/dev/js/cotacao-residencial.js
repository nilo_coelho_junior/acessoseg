function wizardButtonsNameResidencial(index){
	switch (index) {
		case 0:
			$('.cotacao .previous').addClass('disabled')
			break;
		case 1:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').text("Entendi, vamos em frente")
			break;
		case 2:
			$('.cotacao .previous').removeClass('disabled')
			$('.cotacao .next').removeClass('disabled').text("Ufa! Terminamos")
			break;
		default:
			break;
	}
}

$(function() {

	$(".cotacao.residencial").formValidation({
		framework: "bootstrap",
		locale: "pt_BR",
		fields: {
			cep: {
				validators: {
					notEmpty: {
						message: "Insira o cep do imóvel"
					},
					zipCode:{
						country: "BR"
					}
				}
			},			
			tipoDeCondominio: {
				enabled: false
			},
			desejaSeguroParaEquipamentos: {
				enabled: false
			}
		}
	}).on('change', '[name="oImovelEncontraSeDentroDeCondominio"]', function (e) {
		var fv = $(".cotacao.residencial").data("formValidation");
		var taxi = $("input[name='oImovelEncontraSeDentroDeCondominio']:checked").val();

		if (taxi == "Sim") {
			fv.enableFieldValidators('tipoDeCondominio', true).revalidateField('tipoDeCondominio');
			$(".tipoDeCondominio").removeClass('hide')
		} else {
			fv.enableFieldValidators('tipoDeCondominio', false).revalidateField('tipoDeCondominio');
			$(".tipoDeCondominio").addClass('hide')
		}
	}).on('change', '[name="atividadeComercialNoImovel"]', function (e) {
		var fv = $(".cotacao.residencial").data("formValidation");
		var taxi = $("input[name='atividadeComercialNoImovel']:checked").val();

		if (taxi == "Sim") {
			fv.enableFieldValidators('desejaSeguroParaEquipamentos', true).revalidateField('desejaSeguroParaEquipamentos');
			$(".desejaSeguroParaEquipamentos").removeClass('hide')
		} else {
			fv.enableFieldValidators('desejaSeguroParaEquipamentos', false).revalidateField('desejaSeguroParaEquipamentos');
			$(".desejaSeguroParaEquipamentos").addClass('hide')
		}
	})
});