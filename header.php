<!doctype html>
<html class="no-js" lang="pt-BR" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="content-language" content="pt-br" />
    <title>Acesso Seguros</title>
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/dist/img/favicon.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body data-spy="scroll" data-target="#navbar">
    <div class="load-page animated fadeIn">
      <?php get_template_part('templates/loading' ); ?>
    </div>
    <div class="main-page">
      <nav class="navbar navbar-default navbar-fixed-top " id="navbar">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo home_url(); ?>">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/logo.png" alt="">
            </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?php echo home_url(); ?>">Home<span class="sr-only">(current)</span></a></li>
              <li><a href="<?php echo home_url(); ?>/#seguro-auto" data-target="#seguro-auto">Por quê ter um Seguro Auto?</a></li>
              <li><a href="<?php echo home_url(); ?>/#porque-acesso" data-target="#porque-acesso">Porque a Acesso?</a></li>
              <li><a href="<?php echo home_url(); ?>/#seguros" data-target="#seguros">Seguros</a></li>
              <li><a href="<?php echo home_url(); ?>/#depoimentos" data-target="#depoimentos">Depoimentos</a></li>
              <li><a href="<?php echo home_url(); ?>/#blog" data-target="#blog">Blog</a></li>
              <li><a href="<?php echo home_url(); ?>/#faq" data-target="#faq">Dúvidas?</a></li>
              <li><a href="<?php echo home_url(); ?>/#contato" data-target="#contato">Contato</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="https://www.facebook.com/acessoseguros" target="_blank"><span class="icon-facebook"></span></a></li>
              <li><a href="https://www.instagram.com/acessoseguros/" target="_blank"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
      </nav>