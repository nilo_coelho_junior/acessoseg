<?php 

get_header();

if (do_shortcode('[types field="icone-com-brilho"]') == 1){
	$with_brightness = "brightness";
}else{
	$with_brightness = "";
}

?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="container-fluid bg-default page-insurance" style="background-image: url(<?php the_post_thumbnail_url("full"); ?>)">
	  <div class="container">
	    <div class="col-lg-12 page-insurance-header">
	      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
	        <h1 class="title">Seguro <?php echo the_title(); ?></h1>
	        <h4 class="description"><?php echo the_content(); ?></h4>
	        <br>
	        <a class="btn btn-flat btn-warning" href="cotacao">Quero fazer uma cotação!</a>
	        <br>
	      </div>
	      <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center feature-image">
	        <?php echo do_shortcode('[types field="icone" id="{the_id()}" class="'.$with_brightness.'"]'); ?>
	      </div>
	    </div>
	    <span class="icon-chevron visible-lg animated infinite slideInDownCustom"></span>
	  </div>
	</div>
	
	<?php 
		$coberturas = types_child_posts('cobertura');
		$count = count($coberturas);
    $index = 0;
	?>
	<div class="container-fluid wrapper success">
	  <div class="container">
	    <div class="col-lg-12">
	      <h2 class="text-center col-lg-12 subtitle">Coberturas e Assistências</h2>
	      <div class="row content-box hidden-xs">
	      	<?php foreach ($coberturas as $cobertura) {  ?>
	        <div class="<?php echo classInsurances($count); ?> item text-center">
	        	<?php echo get_the_post_thumbnail( $cobertura->ID, 'small', Array('class' => 'flat', 'height' => '100')); ?>
	          <h3><?php echo $cobertura->post_title; ?></h3>
	        </div>
	        <?php } ?>
	      </div>

	      <div class="carousel-page-insurances visible-xs">
		      <?php foreach ($coberturas as $cobertura) {  ?>
		        <div>
		          <img src="<?php the_post_thumbnail_url("full"); ?>" class="flat" alt="">
		          <h3><?php echo $cobertura->post_title; ?></h3>
		        </div>
		      <?php } ?>
		    </div>
				
				<div class="col-lg-12 text-center">
					<p><?php echo do_shortcode('[types field="observacao" id="{the_id()}"]'); ?></p>
				</div>
	    </div>
	  </div>
	</div>
	
	<div class="container-fluid wrapper page-insurance-quickly">
		<div class="container text-center">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/quickly.png" alt="">
			<h2>Contrate já! É rapidinho, só 3 minutinhos.</h2>
			<br>
			<p>Em poucos cliques você preenche e escolhe o seguro que quer contratar e nós te entregamos a melhor proposta personalizada.</p> 
			<p>Com a sua cara! É só seguir o passo a passo pro dia começar mais seguro amanhã! </p>
			<br>
			<a class="btn btn-flat btn-warning" href="cotacao">Quero fazer uma cotação!</a>
		</div>
	</div>

<?php endwhile; endif; ?>

<?php get_template_part('templates/faq'); ?>

<?php get_footer(); ?>