<div class="col-lg-4">
	<?php 
		  $query = new WP_Query(array('post_type' => 'seguro', 'name' => 'auto' ));

		  if ($query->have_posts()): 
		    while($query->have_posts()) : $query->the_post();  
	?>
	<div class="col-lg-12 widget widget-insurance" style="background-image: url(<?php echo the_post_thumbnail_url("large"); ?>)">
		<h2 class="text-center title col-lg-12">Seguro <?php the_title(); ?></h2>
		<?php echo do_shortcode('[types field="icone" id="{the_id()}" class="flat"]') ?>
	  <p class="text-center col-lg-12"><?php echo the_content(); ?></p>
	  <a class="btn btn-warning btn-lg" href="<?php echo get_permalink($child->ID); ?>">Quero fazer uma cotação!</a>
	</div>
	<?php 
			endwhile; 
		endif;
	?>
	
	<?php 
    $query = new WP_Query(array('post_type' => 'post', 'orderby' => 'date', 'posts_per_page' => 5));
    
    if ($query->have_posts()):
  ?>

  <div class="col-lg-12 widget widget-recent-posts">
    <div class="row">
    	<h2 class="title">Últimas</h2>
    	<ul>
	    	<?php while($query->have_posts()) : $query->the_post(); ?>
		    	<li>
			    	<a href="<?php the_permalink(); ?>">
			        <p class="title"><?php the_title(); ?></p>
			      </a>
		      </li>
	      <?php endwhile; ?>
      </ul>
    </div>
  </div>
  <?php 
      wp_reset_query();
    endif;
  ?>

</div>