<?php 
get_header(); 
?>

<div class="container-fluid wrapper primary-2 text-center">
	<img class="flat" src="<?php echo get_template_directory_uri(); ?>/assets/dist/img/cotacao.png" alt="">
	<h1><strong>RECEBEMOS SUA COTAÇÃO!</strong></h1>
	<h3>Aguarde, entraremos em contato com cotações das melhores seguradoras.</h3>
</div>

<?php include_once "templates/faq.php"; ?>

<?php get_footer(); ?>