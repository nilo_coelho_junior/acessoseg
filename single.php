<?php get_header(); ?>
<br>
<div class="container blog post">
	<div class="col-lg-8">
		<?php if ( have_posts() ) : ?>
	    <?php while ( have_posts() ) : the_post(); ?>
	    	<?php echo the_post_thumbnail('full'); ?>
	    	<h1><strong><?php the_title(); ?></strong></h1>
	    	<span><?php the_time('j \d\e F \d\e Y'); ?></span>
	    	<hr>
	      <?php echo the_content(); ?>
	    <?php endwhile; ?>
	  <?php endif; ?>
	  <hr>
	  <div class="col-lg-12">
	  	<div class="row">
		  	<?php 
		  		echo do_shortcode("[fbcomments url='".get_permalink()."' width='100%'' count='off' num='3']"); 
		  	?>
	  	</div>
	  </div>
  </div>
  <?php 
  	wp_reset_query();
  	get_sidebar(); 
  ?>
</div>

<?php get_footer(); ?>