<div class="col-lg-4 col-md-6 col-sm-6 item">
  <a href="<?php the_permalink(); ?>">
    <div class="row">
      <div class="col-lg-12 image-feature">
        <?php the_post_thumbnail('medium'); ?>
      </div>
      <div class="col-lg-12 text-feature">
        <span><?php the_time('j \d\e F \d\e Y'); ?></span>
        <h4 class="title"><strong><?php the_title(); ?></strong></h4>
        <p class="excerpt"><?php echo excerpt_max_charlength(200); ?></p>
      </div>
    </div>
  </a>
</div>