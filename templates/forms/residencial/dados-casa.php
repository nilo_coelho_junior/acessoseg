<h2 class="text-center"><strong>Legal vai ser rapidinho, vamos lá ...</strong></h2>
<br>

<div class="form-group">
  <label for="tipoDeConstrucao" class="col-sm-6 control-label">Tipo de construção da casa</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="tipoDeConstrucao" id="tipoDeConstrucao" value="Alvenaria" required> Alvenaria
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="tipoDeConstrucao" id="tipoDeConstrucao" value="Madeira"> Madeira
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="tipoDeConstrucao" id="tipoDeConstrucao" value="Construção Mista (Alvenaria e com mais de 25% de Madeira)"> Construção Mista (Alvenaria e com mais de 25% de Madeira)
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="tipoDeConstrucao" id="tipoDeConstrucao" value="Construção Mista (Alvenaria e com menos de 25% de Madeira)"> Construção Mista (Alvenaria e com menos de 25% de Madeira)
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="formaDeUtilizacao" class="col-sm-6 control-label">Forma de utilização</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="formaDeUtilizacao" id="formaDeUtilizacao" value="Moro nela" required> Moro nela
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="formaDeUtilizacao" id="formaDeUtilizacao" value="Uso eventualmente (Finais de semana, temporada e férias)"> Uso eventualmente (Finais de semana, temporada e férias)
			</label>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="cepDoImóvel" class="col-sm-6 control-label">Qual é o CEP do imóvel? </label>
  <div class="col-sm-6">
  	<div class="row">
	  	<div class="col-sm-4">
				<input type="text" data-mask="cep-autocomplete" class="form-control" name="cepDoImóvel" id="cepDoImóvel" placeholder="CEP" required>
			</div>
			<a class="col-sm-8" href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">
				<h5>Não sei o CEP</h5>
			</a>
		</div>
  </div>
</div>

<div class="form-group">
  <label for="endereco" class="col-sm-6 control-label">Endereço</label>
  <div class="col-sm-6">
    <input type="text" class="form-control" id="endereco" name="endereco" required>
  </div>
</div>

<div class="form-group">
  <label for="numero" class="col-sm-6 control-label">Número</label>
  <div class="col-sm-2">
    <input type="text" class="form-control" id="numero" name="numero" required>
  </div>
</div>

<div class="form-group">
  <label for="complemento" class="col-sm-6 control-label">Complemento</label>
  <div class="col-sm-6">
    <input type="text" class="form-control" id="complemento" name="complemento">
  </div>
</div>

<div class="form-group">
  <label for="bairro" class="col-sm-6 control-label">Bairro</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="bairro" name="bairro" required>
  </div>
</div>

<div class="form-group">
  <label for="cidade" class="col-sm-6 control-label">Cidade</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" id="cidade" name="cidade" required>
  </div>
</div>

<div class="form-group">
  <label for="estado" class="col-sm-6 control-label">Estado</label>
  <div class="col-sm-3">
    <select class="form-control" name="estado" required>
    	<option selected disabled>Selecione um Estado</option>
			<option value="AC">AC</option>
			<option value="AL">AL</option>
			<option value="AP">AP</option>
			<option value="AM">AM</option>
			<option value="BA">BA</option>
			<option value="DF">DF</option>
			<option value="CE">CE</option>
			<option value="ES">ES</option>
			<option value="GO">GO</option>
			<option value="MA">MA</option>
			<option value="MT">MT</option>
			<option value="MS">MS</option>
			<option value="MG">MG</option>
			<option value="PA">PA</option>
			<option value="PB">PB</option>
			<option value="PR">PR</option>
			<option value="PE">PE</option>
			<option value="PI">PI</option>
			<option value="RJ">RJ</option>
			<option value="RN">RN</option>
			<option value="RS">RS</option>
			<option value="RO">RO</option>
			<option value="RR">RR</option>
			<option value="SC">SC</option>
			<option value="SP">SP</option>
			<option value="SE">SE</option>
			<option value="TO">TO</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="limiteMaximoDaIndenizacao" class="col-sm-6 control-label">Valor do imóvel para reconstrução e reposição dos bens internos</label>
  <div class="col-sm-4">
    <input type="text" data-mask='currency' class="form-control" id="limiteMaximoDaIndenizacao" name="limiteMaximoDaIndenizacao" aria-describedby="helpBlock" required>
    <span id="helpBlock" class="help-block">O valor a ser considerado é aquele necessário para reconstrução do imóvel e reposição dos seus bens internos no caso de um acidente que cause a destruição completa da sua residência. Não leve em conta o valor do imóvel no mercado imobiliário, isto é, considere o valor para reconstrução e não o valor de compra/venda do imóvel.</span>
  </div>
</div>