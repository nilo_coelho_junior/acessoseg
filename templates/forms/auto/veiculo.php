<h2 class="text-center"><strong>Agora, fale um pouquinho do carro</strong></h2>
<input type="hidden" name="section" value="Agora fale um pouquinho do carro">
<br>
<p><strong>Atenção:</strong> Continue preenchendo apenas se o seu veículo <strong>não é blindado, de empresa, adaptado para deficiente físico, moto, caminhão ou com chassi remarcado.</strong> Se você se encaixa em alguma dessas situações, ligue para nós no 0800 086 6464 para que possamos ajudá-lo.</p>
<br>
<div class="form-group">
  <label for="marca" class="col-sm-6 control-label">Qual é a marca do carro?</label>
  <div class="col-sm-4">
    <select class="form-control marcas" name="marca" required>
    	<option>Carregando...</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label for="modelo" class="col-sm-6 control-label">Qual o modelo?</label>
  <div class="col-sm-4">
    <select class="form-control modelos" name="modelo" required>
    	<option selected disabled="">Selecione uma marca primeiro</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="anoFabricacao" class="col-sm-6 control-label">Ano do modelo?</label>
  <div class="col-sm-4">
    <select class="form-control anos" name="anoFabricacao" required>
    	<option selected disabled>Selecione um modelo primeiro</option>
    </select>
  </div>
</div>

<div class="form-group">
  <label for="anoFrabicacaoVeículo" class="col-sm-6 control-label">Ano de Fabricação do Modelo</label>
  <div class="col-sm-4">
    <input type="text" data-mask="year" class="form-control" name="anoFrabicacaoVeículo" id="anoFrabicacaoVeículo" placeholder="" required>
  </div>
</div>

<div class="form-group">
  <label for="placa" class="col-sm-6 control-label">Placa</label>
  <div class="col-sm-4">
    <input data-mask="board" type="text" class="form-control" name="placa" id="placa" placeholder="" required>
  </div>
</div>

<div class="form-group">
  <label for="chassi" class="col-sm-6 control-label">
    Chassi
    <small>Essa informação não é obrigatória, porém algumas seguradoras exigem essa informação na hora da cotação do seu seguro. Para conseguir analisar os preços em todas as seguradoras, preencha essa informação</small>
  </label>
  <div class="col-sm-4">
    <input type="text" class="form-control" name="chassi" id="chassi" placeholder="">
  </div>
</div>

<div class="form-group 0km hide">
  <label for="0km" class="col-sm-6 control-label">O veículo é 0km?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default">
        <input type="radio" name="0km" id="0km" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="0km" id="0km" value="Não"> Não
      </label>      
    </div>
  </div>
</div>

<div class="form-group">
  <label for="possuiSeguro" class="col-sm-6 control-label">O veículo já possui seguro?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="possuiSeguro" id="possuiSeguro" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="possuiSeguro" id="possuiSeguro" value="Não"> Não
      </label>      
    </div>
  </div>
</div>

<div class="form-group mesmoModeloDaApoliceAnterior hide">
  <label for="mesmoModeloDaApoliceAnterior" class="col-sm-6 control-label">O modelo do carro cotado é o mesmo do veículo que está em sua apolice?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="mesmoModeloDaApoliceAnterior" id="mesmoModeloDaApoliceAnterior" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="mesmoModeloDaApoliceAnterior" id="mesmoModeloDaApoliceAnterior" value="Não"> Não
      </label>      
    </div>
  </div>
</div>

<div class="form-group seguradoraAnterior hide">
  <label for="seguradoraAnterior" class="col-sm-6 control-label">Qual a seguradora anterior?</label>
  <div class="col-sm-4">
    <select class="form-control seguradoraAnterior" name="seguradoraAnterior" id="seguradoraAnterior" required>
      <option selected disabled="">Selecione uma seguradora</option>
      <option value="Allianz">Allianz</option>
      <option value="AzulSeguros">Azul Seguros</option>
      <option value="Bradesco Seguros">Bradesco Seguros</option>
      <option value="HDISeguros">HDI Seguros</option>
      <option value="ItauSeguros">Itaú Seguros</option>
      <option value="LibertySeguros">Liberty Seguros</option>
      <option value="MapfreSeguros">Mapfre Seguros</option>
      <option value="NobreSeguradora">Nobre Seguradora</option>
      <option value="PortoSeguro">Porto Seguro</option>
      <option value="SulAmerica">Sul América</option>
      <option value="Tokio Marine">Tokio Marine</option>
      <option value="Outra">Outra</option>
    </select>
  </div>
</div>

<div class="form-group classeBonusApoliceAnterior hide">
  <label for="classeBonusApoliceAnterior" class="col-sm-6 control-label">
    Qual a classe de bônus na apólice que você possui?
    <small>
      Obs: A classe de bônus é um desconto progressivo que aumenta em toda a renovação de seguro sem sinistro. Informe aqui a classe de bônus que consta em sua apólice de seguro atual.
    </small>
  </label>
  <div class="col-sm-4">
    <select class="form-control classeBonusApoliceAnterior" name="classeBonusApoliceAnterior" id="classeBonusApoliceAnterior" required>
      <option selected disabled="">Selecione uma classe</option>
      <option value="naoSeiMinhaClasseDeBonus">Não sei minha classe de bônus</option>
      <option value="semBonus">Sem bônus</option>
      <option value="classe1">Classe 1</option>
      <option value="classe2">Classe 2</option>
      <option value="classe3">Classe 3</option>
      <option value="classe4">Classe 4</option>
      <option value="classe5">Classe 5</option>
      <option value="classe6">Classe 6</option>
      <option value="classe7">Classe 7</option>
      <option value="classe8">Classe 8</option>
      <option value="classe9">Classe 9</option>
      <option value="classe10">Classe 10</option>
    </select>
  </div>
</div>

<div class="form-group quantosSinistrosOcorreram hide">
  <label for="quantosSinistrosOcorreram" class="col-sm-6 control-label">
    Quantos sinistros ocorreram durante a vigência desta apólice?
    <small>
      Obs: Sinistro é um incidente (batida, roubo, etc.) que foi notificado e indenizado pela seguradora. Não é caracterizado sinistro o uso de assistência 24h, guincho ou reparo de vidros.
    </small>
  </label>
  <div class="col-sm-4">
    <select class="form-control quantosSinistrosOcorreram" name="quantosSinistrosOcorreram" id="quantosSinistrosOcorreram" required>
      <option selected disabled="">Selecione uma opção</option>
      <option value="nenhum">Nenhum</option>
      <option value="1Sinistro">1 Sinistro</option>
      <option value="2Sinistro">2 Sinistros</option>
      <option value="3Sinistro">3 Sinistros</option>
      <option value="4Sinistro">4 Sinistros</option>
      <option value="5Sinistro">5 Sinistros</option>
    </select>
  </div>
</div>

<div class="form-group veiculoRoubadoNosUltimos2Anos hide">
  <label for="veiculoRoubadoNosUltimos2Anos" class="col-sm-6 control-label">O segurado teve algum veículo roubado nos últimos 2 (dois) anos?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="veiculoRoubadoNosUltimos2Anos" id="veiculoRoubadoNosUltimos2Anos" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="veiculoRoubadoNosUltimos2Anos" id="veiculoRoubadoNosUltimos2Anos" value="Não"> Não
      </label>      
    </div>
  </div>
</div>

<div class="form-group bancoQueTemConta hide">
  <label for="bancoQueTemConta" class="col-sm-6 control-label">
    Banco que tem conta (Opcional)
    <small>
      Obs: Algumas seguradoras oferecem melhores condições de pagamento de acordo com o seu banco.
    </small>
  </label>
  <div class="col-sm-4">
    <select class="form-control bancoQueTemConta" name="bancoQueTemConta" id="bancoQueTemConta">
      <option selected disabled="">Selecione um banco</option>
      <option value="BancoDoBrasil">Banco do Brasil</option>
      <option value="Bradesco">Bradesco</option>
      <option value="Itau">Itaú</option>
      <option value="Santander">Santander</option>
      <option value="Outro">Outro</option>
    </select>
  </div>
</div>

<div class="form-group vencimentoDaApolice hide">
  <label for="vencimentoDaApolice" class="col-sm-6 control-label">Data de vencimento da apólice</label>
  <div class="col-sm-4">
    <input type="text" class="form-control" name="vencimentoDaApolice" id="vencimentoDaApolice" data-mask="date" placeholder="dd/mm/aaaa" required>
  </div>
</div>

<div class="form-group">
  <label for="alienado" class="col-sm-6 control-label">O carro é alienado?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="alienado" id="alienado" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="alienado" id="alienado" value="Não"> Não
      </label>      
    </div>
  </div>
</div>
<div class="form-group">
  <label for="kitGas" class="col-sm-6 control-label">Tem kit-gás?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="kitGas" id="kitGas" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="kitGas" id="kitGas" value="Não"> Não
      </label>      
    </div>
  </div>
</div>
<div class="form-group incluirKitGas hide">
  <label for="incluirKitGas" class="col-sm-6 control-label">Deseja incluir na cobertura?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="incluirKitGas" id="incluirKitGas" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="incluirKitGas" id="incluirKitGas" value="Não"> Não
      </label>      
    </div>
  </div>
</div>
<div class="form-group incluirKitGasValor hide">
  <label for="incluirKitGasValor" class="col-sm-6 control-label">Qual é o valor?</label>
  <div class="col-sm-2">
		<input type="text" data-mask='currency' class="form-control" name="incluirKitGasValor" id="incluirKitGasValor" required>
  </div>
</div>
<div class="form-group">
  <label for="taxi" class="col-sm-6 control-label">O veículo é um taxi?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="taxi" id="taxi" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="taxi" id="taxi" value="Não"> Não
      </label>      
    </div>
  </div>
</div>
<div class="form-group taxiIsencaoImposto hide">
  <label for="taxiIsencaoImposto" class="col-sm-6 control-label">Possui isenção de imposto?</label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
		  <label class="btn btn-default">
			  <input type="radio" name="taxiIsencaoImposto" id="taxiIsencaoImposto" value="ICMS e IPI" required> ICMS e IPI
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="taxiIsencaoImposto" id="taxiIsencaoImposto" value="ICMS"> ICMS
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="taxiIsencaoImposto" id="taxiIsencaoImposto" value="IPI"> IPI
			</label>
			<label class="btn btn-default">
			  <input type="radio" name="taxiIsencaoImposto" id="taxiIsencaoImposto" value="Não"> Não possuo
			</label>
		</div>
  </div>
</div>
<div class="form-group">
  <label for="possuiAlgumDispositivoAntifurto" class="col-sm-6 control-label">O veículo possui algum dispositivo antifurto? </label>
  <div class="col-sm-4">
    <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default ">
        <input type="radio" name="possuiAlgumDispositivoAntifurto" id="possuiAlgumDispositivoAntifurto" value="Sim" required> Sim
      </label>
      <label class="btn btn-default">
        <input type="radio" name="possuiAlgumDispositivoAntifurto" id="possuiAlgumDispositivoAntifurto" value="Não"> Não
      </label>      
    </div>
  </div>
</div>
<div class="form-group quaisDispositivosAntifurto hide">
  <label for="quaisDispositivosAntifurto" class="col-sm-6 control-label">Quais desses dispositivo antifurto seu veículo possui? </label>
  <div class="col-sm-4 checkbox">
    <label>
      <input type="checkbox" name="quaisDispositivosAntifurto" value="Alarme" required> Alarme
    </label>
    <label>
      <input type="checkbox" name="quaisDispositivosAntifurto" value="Bloqueador" required> Bloqueador
    </label>
    <label>
      <input type="checkbox" name="quaisDispositivosAntifurto" value="Rastreador" required> Rastreador
    </label>
    <label>
      <input type="checkbox" name="quaisDispositivosAntifurto" value="Outros" required> Outros
    </label>
  </div>
</div>