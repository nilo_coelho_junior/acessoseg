<?php 

get_header(); 

$parent = get_post($post->post_parent);

?>

<form action="" class="cotacao <?php echo $parent->post_name; ?> form-horizontal">
  <?php wp_nonce_field('send_cotation', 'token'); ?>
  <input type="hidden" name="page" value="<?php echo $parent->post_name; ?>">
  <?php get_template_part("templates/forms/".$parent->post_name."/form");  ?>

</form>

<?php

get_footer(); 

?>